# WeatherApi

## Developer: Salman Lashkarara

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.10.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

This application is a frontend to get the updated weather forecast for certain areas. It works using some REST services to provide the information for the user. On page load it provides the weather statistics for the current location of user. Additionally, the user can add some cities to get the information or remove the selected ones. The navigation tool only shows a sample of routing in a single-web application.

The technologies to develop this application are: **Angular2**, **Typescript**, **HTML5**, **CSS3**, and **REST**. 

The application works quite simple. The main component is the SearchComponent. It contains a form, and an unordered list <ul> to present the cities.  There are two services too. One for http requests, and other one to set the received information.

 When the application loads for the first time, it sends a request to a REST service to achieve the geolocation of the device. The backend server does it based on the IP or the user’s device. This endpoint  is [http://ip-api.com/json](Link URL). Once it receives the geolocation of the user, then it sends a request to get weather statistic. The endpoint for weather forecast is [http://api.openweathermap.org/data/2.5/weather?q=](Link URL) . To keep the list of added cities, we use an array of string in the SearchComponent (let’s call it cities ). By clicking on the cross icon, it simply omits an element from cities array.
The main challenge was in recognizing the name of the city. The backend weather api almost response to all the requests. For instance, if you insert **jjj** as the name of the city, it provides a valid response for the city **Chandannath**   Also, the backend is not smart enough, to make a guess a bout miss-spelled name of cities. For instance, the **Berlinx** is respond with **Tiergarten**. Therefore, we decided to compare the sent and received name of the city as a metric for validation. As a result, the application is quite sensitive to the spell.