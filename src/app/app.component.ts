import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES} from '@angular/router';
import {MenuComponent} from './shared/menu.component';
import {FooterComponent} from './shared/footer.component';
import {WeatherService} from './app.service';
import {Weather} from './weather';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  template: `
            <nav-menu></nav-menu>
            <div class="container">
                <router-outlet></router-outlet>
            </div>
            <footer></footer>`,
  styleUrls: ['app.component.css'],
  providers:[WeatherService],
  directives:[ROUTER_DIRECTIVES,MenuComponent,FooterComponent],
})

export class AppComponent {
 
  weather:Weather;
  constructor(private _WeatherService : WeatherService){} 

    ngOnInit(){
        this._WeatherService.setWeatherForcastByLocation();
        setTimeout(()=>{
            this.weather=this._WeatherService.getWeatherStatistic();}, 3000);
         setTimeout(()=> {alert('Weather in:'+this.weather.name+'\n'+
                                'Main: '+this.weather.main+'\n'+
                                'Humidity:'+this.weather.humidity)+'\n'+
                                'Tempeture:'+this.weather.temp;}, 5000);   
    }
}

