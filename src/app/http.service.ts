import { Injectable } from '@angular/core';
import {Http} from '@angular/http';


const APPID="cd723a8ae961ce17ae97d6ea7ba6f787";


@Injectable()
export class HttpService {

  
    constructor(private _http:Http){ }

    getGeoLocation(){
      return  this._http.get("http://ip-api.com/json");
    }

    getWeatherForcastByLocation(lat, lon){
            return this._http.get("http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&APPID="+APPID);
    }

    getWeatherForcastByCityName(city:string){
             return this._http.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&APPID="+APPID);                    
    }

}
