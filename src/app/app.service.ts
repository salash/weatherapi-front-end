import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Weather} from './weather';
import {HttpService} from './http.service';


@Injectable()
export class WeatherService{
    private lat:string;
    private lon:string;
    private weatherStatatistic:Weather;

    constructor(private _http:HttpService){ }

    setGeoLocation(){
        this._http.getGeoLocation()
            .subscribe(res =>{
                            this.lat=res.json().lat;
                            this.lon=res.json().lon;});
    }

    setWeatherForcastByLocation(){
        this.setGeoLocation();
        setTimeout(() => {
                this._http.getWeatherForcastByLocation(this.lat,this.lon)
                        .subscribe( res =>this.setItems(res.json()));               
        }, 2000);
    }

    setWeatherForcastByCityName(city:string){
         this._http.getWeatherForcastByCityName(city)
                  .subscribe( res => this.setItems(res.json()));             
    }

    getWeatherStatistic(){
        return this.weatherStatatistic;
    }

    setItems(res){
        if(res.cod=="404")
            this.weatherStatatistic=new Weather('',0,'','','','',"City Not Found");                             
        else
            this.weatherStatatistic=new Weather(res.weather[0].main,
                                                this.KelivinToCelsius(res.main.temp),
                                                res.main.pressure,
                                                res.main.humidity,
                                                res.name,
                                                res.weather[0].icon,null); 
    }

    KelivinToCelsius (k){
       return Math.round(k-273.15)
    }
}