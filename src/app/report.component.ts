import { Component, Input,EventEmitter,Output } from '@angular/core';

@Component({
    selector:'report',
    template:` 
            <div class="panel panel-default">
                <div class="panel-heading">
                        Weather in: {{cityName}}
                        <span class="glyphicon glyphicon-remove" (click)="OnDelete()" style="float: right;"></span>
                </div>
                    
                <div class="panel-body">Weather: <img [src]="iconLink"> 
                | Tempeture: {{temp}}  &#8451;  | Pressure: {{pressure}}hPa | Humidity: {{humidity}}%</div>
            </div>`
})

export class ReportComponent{
    @Output() SelectToDelete=new EventEmitter();

    @Input() cityName;
    @Input() main;
    @Input() temp;
    @Input() pressure;
    @Input() humidity;
    @Input() iconLink;

    OnDelete(){
        console.log("inside delete!")
        this.SelectToDelete.emit(this.cityName);
    }
}