import {Component} from '@angular/core';

@Component({
    selector: 'about',
    template:`<div style="height:500px;">
                <div class="container">        
                        <div class="row">
                            <div class="col-lg-5" stye="max-width:100%; height:auto; margin:5px;">
                                <img src="./app/shared/pic/Main.jpg" >
                            </div>
                            <div class="col-lg-5" style="float:right; max-width:100%; height:auto;">

                                <p>This application is developed by <strong>Salman Lashkarara.</strong>
It is a simple front-end which commiuncates with a REST <strong> weather forcast service</strong> in the backend.&nbsp.
Also, we used another application to obtain geolocation based on IP address of device, then on page load, 
the application report the weather statistic in the current location of device.
The&nbsp;<strong>Angularjs2</strong>&nbsp;is the most important technology in use.
To see more projects from me please visit <a href="https://www.linkedin.com/in/salman-lashkarara-20013081">my linkdin</a>.</p>

                            </div>
                        </div>     
                </div>
            </div>`
})

export class AboutComponent{
    
}