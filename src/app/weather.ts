export class  Weather{
    main: string;
    temp:number;
    pressure:string;
    humidity:string;
    name:string;
    iconLink:string;
    error:string;
   

    constructor(main:string,temp:number,pressure:string,humidity:string,name:string,iconId:string,error:string ){
        this.main=main;
        this.temp=temp;
        this.pressure=pressure;
        this.humidity=humidity;
        this.name=name;
        this.iconLink= "http://openweathermap.org/img/w/"+iconId+".png";
        this.error=error;
    }
    
    printer(){
        console.log("main: "+this.main+
        " temp: "+this.temp+
        " pressure: "+this.pressure+
        " humidity: "+this.humidity+
        " Name: "+this.name+
        " Icon: "+this.iconLink+
        " Error: "+this.error);
    }
} 