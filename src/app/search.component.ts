import {Component} from '@angular/core';
import {ReportComponent} from './report.component';
import {WeatherService} from './app.service';
import {Weather} from './weather';

@Component({
    selector:'search-form',
    templateUrl:'./app/search.component.html' ,
    providers:[WeatherService],
    directives:[ReportComponent]
})
export class SearchComponent{
    
    private weather: Weather; 
    private cities:  Weather[];
    private cityName:string;

    private isFound=true;
    private isLoading=false;
  
     constructor(private _WeatherService : WeatherService){
         this.cities=[];
     }
       
    submit(){
        localStorage.setItem("city", this.cityName);
        this._WeatherService.setWeatherForcastByCityName(this.cityName);
        this.isLoading=true;
        setTimeout(()=>{
            this.weather=this._WeatherService.getWeatherStatistic();
         
            if(localStorage.getItem("city").toLowerCase() ==this.weather.name.toLowerCase()){
                this.cities.push(this.weather); 
                this.ConfigPresentation(true,false);
            }else
                this.ConfigPresentation(false,false);
                
        }, 3000);    
    }

    ConfigPresentation(found,load){
        this.isFound=found;
        this.isLoading=load;
    }

    OnDelete(value){
        for(let i=0;i<this.cities.length ;i++)
             if(this.cities[i].name==value) 
                 this.cities.splice(i, 1);
    }
}
