import {Component} from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector:'nav-menu',
    template:`
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" [routerLink]="['/home']">Proekspert</a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="active"><a [routerLink]="['/home']">Home </a></li>
                                <li><a [routerLink]="['/about']">About this</a></li>
                            </ul>
                    
                        </div>
                    </div>
                </div>    
            </nav>`,
            directives:[ROUTER_DIRECTIVES]
})

export class MenuComponent{

}