import { provideRouter }  from '@angular/router';
import { SearchComponent }   from './search.component';
import { AboutComponent } from './about.component';

const APP_ROUTES=[
    {path:'', component:SearchComponent},
    {path:'about', component:AboutComponent},
    {path:'**', component:SearchComponent }];

export const ROUTER_PROVIDER=[provideRouter(APP_ROUTES)];