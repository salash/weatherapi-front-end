import { bootstrap } from '@angular/platform-browser-dynamic';
import {HTTP_PROVIDERS} from '@angular/http';
import {ROUTER_PROVIDER} from './app/app.routes';

import { ROUTER_DIRECTIVES } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { AppComponent, environment } from './app/';
import {HttpService} from './app/http.service';

if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent,[ROUTER_PROVIDER,ROUTER_DIRECTIVES,HTTP_PROVIDERS,HttpService]);
