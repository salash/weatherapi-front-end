import { WeatherApiPage } from './app.po';

describe('weather-api App', function() {
  let page: WeatherApiPage;

  beforeEach(() => {
    page = new WeatherApiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
